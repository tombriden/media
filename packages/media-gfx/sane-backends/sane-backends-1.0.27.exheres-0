# Copyright 2008, 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'sane-backends-1.0.19-r2.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ] systemd-service udev-rules

SUMMARY="Scanner Access Now Easy - Backends"
HOMEPAGE="http://www.sane-project.org"
DOWNLOADS="https://alioth.debian.org/frs/download.php/file/4224/${PNV}.tar.gz"

REMOTE_IDS="freecode:sane"

LICENCES="GPL-2 public-domain"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    avahi
    gphoto2
    ieee1284 [[ description = [ Add parallel port support ] ]]
    systemd
    tiff
    usb
    v4l
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# tests try to access usb
RESTRICT="test"

# TODO: net-snmp
DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.18.1]
        virtual/pkg-config[>=0.9.0]
    build+run:
        group/scanner
        avahi? ( net-dns/avahi[>=0.6.24] )
        gphoto2? ( media-libs/libgphoto2 )
        ieee1284? ( sys-libs/libieee1284 )
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=6b] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        systemd? ( sys-apps/systemd )
        tiff? ( media-libs/tiff )
        usb? ( virtual/usb:1 )
        v4l? ( media-libs/v4l-utils )
    run:
        user/saned
        group/saned
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-ipv6
    --enable-locking
    --enable-nls
    --with-docdir=/usr/share/doc/${PNVR}
    --with-group=scanner
    --with-lockdir=/run/lock/sane
    --without-api-spec
    --without-snmp
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    avahi
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    gphoto2
    systemd
    v4l
    usb
)

pkg_pretend() {
    if [[ -f "${ROOT}"/etc/tmpfiles.d/sane.conf ]] ; then
        ewarn "The configuration file /etc/tmpfiles.d/sane.conf has been moved to"
        ewarn "/usr/$(exhost --target)/lib/tmpfiles.d/sane.conf and can be safely removed after"
        ewarn "upgrade if you did not make any changes to it."
    fi
}

src_prepare() {
    expatch -p1 "${FILES}"/0001-Remove-cross-compilation-check.patch

    # workaround invoking chgrp for a redundant locking group availability test
    edo sed \
        -e "/sanetest.file/d" \
        -i configure{,.ac} \
        -i acinclude.m4

    # TODO: Report upstream pkg-config cross/multiarch
    edo sed \
        -e "s:pkg-config:$(exhost --tool-prefix)&:g" \
        -i tools/sane-config.in

    autotools_src_prepare
}

src_install() {
    option usb && DEFAULT_SRC_INSTALL_EXTRA_SUBDIRS+=( tools/hotplug )
    default

    edo rmdir "${IMAGE}"/run/{lock/{sane,},}
    edo rmdir "${IMAGE}"/usr/share/sane/{artec_eplus48u,gt68xx,snapscan,epjitsu,}

    keepdir /etc/sane.d/dll.d
    hereenvd 30sane <<EOF
SANE_CONFIG_DIR=/etc/sane.d
EOF

    dodir "${UDEVRULESDIR}"
    insinto "${UDEVRULESDIR}"
    newins tools/udev/libsane.rules 65-libsane.rules

    edo tools/sane-desc -m hwdb -s "${WORK}/doc/descriptions:${WORK}/doc/descriptions-external" -d0 > tools/udev/sane-backends.hwdb
    dodir "${UDEVHWDBDIR}"
    insinto "${UDEVHWDBDIR}"
    newins tools/udev/sane-backends.hwdb 20-sane-backends.hwdb

    if option usb; then
        insinto /etc/hotplug/usb
        doins tools/hotplug/libsane.usermap

        exeinto /etc/hotplug/usb
        doexe tools/hotplug/libusbscanner
    fi

    install_systemd_files
    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins sane.conf <<EOF
d /run/lock/sane 0770 root scanner
EOF
}

