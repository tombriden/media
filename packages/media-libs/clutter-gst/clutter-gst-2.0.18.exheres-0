# Copyright 2009, 2011 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Library for the Clutter toolkit providing audio/video playback using GStreamer"
HOMEPAGE="http://www.clutter-project.org"

LICENCES="LGPL-2.1"
SLOT="2.0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="gobject-introspection gtk-doc"

DEPENDENCIES="
    build:
        app-doc/gtk-doc-autotools
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.8] )
    build+run:
        dev-libs/glib:2[>=2.18.0]
        x11-libs/clutter:1[>=1.6.0][gobject-introspection?]
        x11-libs/cogl:1.0[>=1.10.0][gobject-introspection?]
        media-libs/gstreamer:1.0[>=1.2.0][gobject-introspection?]
        media-plugins/gst-plugins-base:1.0[>=1.2.0][gobject-introspection?]
        media-plugins/gst-plugins-bad:1.0[>=1.2.0] [[
            note = [ automagic depdency for HW accelerated element support ]
        ]]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.8] )
"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}/0001-docs-make-parallel-installable.patch" )

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gobject-introspection introspection' 'gtk-doc' )

AT_M4DIR=( "${WORK}/build/autotools" )

src_prepare() {
    edo gtkdocize --copy
    autotools_src_prepare
}

