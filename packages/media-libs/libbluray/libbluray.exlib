# Copyright 2011 Alex ELsayed <eternaleye@gmail.com>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    SCM_REPOSITORY="https://git.videolan.org/git/libbluray.git"
    require scm-git autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]
else
    # certificate doesn't include ftp subdomain: https://trac.videolan.org/vlc/ticket/19225
    DOWNLOADS="http://ftp.videolan.org/pub/videolan/${PN}/${PV}/${PNV}.tar.bz2"
fi

export_exlib_phases src_configure

SUMMARY="Open-source library designed for Blu-Ray Discs playback for media players, like VLC or MPlayer"
HOMEPAGE="https://www.videolan.org/developers/${PN}.html"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    doc
    examples
"

DEPENDENCIES="
    build:
        dev-java/apache-ant
        virtual/pkg-config[>=0.9.0]
        doc? ( app-doc/doxygen[dot] )
    build+run:
        dev-libs/libxml2:2.0[>=2.6]
        media-libs/fontconfig
        media-libs/freetype:2
        virtual/jdk:1.8
    suggestion:
        virtual/libaacs [[ description = [ Decrypt encrypted blurays ] ]]
"

# Disable ps and pdf generation due to chronic latex shortage
# dlopen the crypto libs at runtime to avoid build-time automagic checks
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-bdjava-jar
    --disable-doxygen-{pdf,ps}
    --disable-static
    --with-fontconfig
    --with-freetype
    --with-libxml2
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "doc doxygen-doc"
    "doc doxygen-dot"
    examples
)

AT_M4DIR=( m4 )

libbluray_src_configure() {
    export JDK_HOME="/usr/$(exhost --target)/lib/jdk"

    default
}

