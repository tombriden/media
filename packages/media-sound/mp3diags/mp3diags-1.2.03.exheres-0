# Copyright 2011-2016 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'mp3diags-1.0.01.046.ebuild' from Gentoo, which is:
#   Copyright 1999-2009 Gentoo Foundation

MY_PN=${PN/${PN}/MP3Diags}

require qmake [ slot=4 ] sourceforge [ suffix=tar.gz project=${PN} ] freedesktop-desktop gtk-icon-cache

SUMMARY="Qt-based MP3 diagnosis and repair tool"
DOWNLOADS+=" doc? ( mirror://sourceforge/${PN}/${MY_PN}Doc-${PV}.tar.gz )"

LICENCES="GPL-2 || ( GPL-3 LGPL-3 )"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="doc"

DEPENDENCIES="
    build+run:
        dev-libs/boost[>=1.37.0]
        sys-libs/zlib
        x11-libs/qt:4[>=4.3.1]
"

WORK=${WORKBASE}/${MY_PN}-${PV}

src_prepare() {
    default

    edo lrelease-qt4 src/translations/mp3diags_*.ts

    # TODO: report upstream, multiarch search path fix
    edo sed \
        -e 's:/../share/:/../../share/:g' \
        -i  src/Translation.cpp
}

src_install() {
    dobin bin/${MY_PN}

    insinto /usr/share/applications
    doins desktop/${MY_PN}.desktop

    for x in 16 22 24 32 48; do
        insinto /usr/share/icons/hicolor/${x}x${x}/apps
        newins desktop/${MY_PN}${x}.png ${MY_PN}.png
    done

    insinto /usr/share/${PN}/translations
    doins src/translations/*.qm

    if option doc ; then
        insinto /usr/share/doc/${PNVR}/html
        doins -r ../${MY_PN}Doc-${PV}/*
    fi
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

